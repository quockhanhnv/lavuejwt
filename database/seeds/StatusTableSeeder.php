<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
          ['status_name' => 'HighLight', 'color_code' => '#dc3545'],
          ['status_name' => 'High', 'color_code' => '#e4aac5'],
          ['status_name' => 'Normal', 'color_code' => '#337ab7'],
          ['status_name' => 'Low', 'color_code' => '#6c757d'],
        ];
        DB::table('m_statuses')->insert($statuses);
    }
}
