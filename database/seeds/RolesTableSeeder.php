<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // for roles table
        $roles = [
          ['id' => SUPER_ADMIN_ROLE_ID, 'role_name' => 'SuperAdmin', 'description' => 'Role Super Admin'],
          ['id' => ADMIN_ROLE_ID, 'role_name' => 'Admin', 'description' => 'Role Admin'],
          ['id' => USER_ROLE_ID, 'role_name' => 'User', 'description' => 'Role Normal User'],
        ];

        DB::table('m_roles')->insert($roles);

        // for roleRights table
        $roleRights = [
            // for admin
            ['role_id' => ADMIN_ROLE_ID, 'controller' => 'CompanyController', 'description' => 'Access controllers'],
            ['role_id' => ADMIN_ROLE_ID, 'controller' => 'ProjectController', 'description' => 'Access controllers'],
            ['role_id' => ADMIN_ROLE_ID, 'controller' => 'ProjectMemberController', 'description' => 'Access controllers'],
            ['role_id' => ADMIN_ROLE_ID, 'controller' => 'UserController', 'description' => 'Access controllers'],
            ['role_id' => ADMIN_ROLE_ID, 'controller' => 'InquiryController', 'description' => 'Access controllers'],
            ['role_id' => ADMIN_ROLE_ID, 'controller' => 'CompanyController', 'description' => 'Access controllers'],
            ['role_id' => ADMIN_ROLE_ID, 'controller' => 'UserController', 'description' => 'Access controllers'],
            ['role_id' => ADMIN_ROLE_ID, 'controller' => 'StatusController', 'description' => 'Access controllers'],

            // for super admin
            ['role_id' => SUPER_ADMIN_ROLE_ID, 'controller' => 'CompanyController', 'description' => 'Access controllers'],
            ['role_id' => SUPER_ADMIN_ROLE_ID, 'controller' => 'ProjectController', 'description' => 'Access controllers'],
            ['role_id' => SUPER_ADMIN_ROLE_ID, 'controller' => 'ProjectMemberController', 'description' => 'Access controllers'],
            ['role_id' => SUPER_ADMIN_ROLE_ID, 'controller' => 'UserController', 'description' => 'Access controllers'],
            ['role_id' => SUPER_ADMIN_ROLE_ID, 'controller' => 'StatusController', 'description' => 'Access controllers'],
            ['role_id' => SUPER_ADMIN_ROLE_ID, 'controller' => 'InquiryController', 'description' => 'Access controllers'],
        ];

        DB::table('m_role_rights')->insert($roleRights);


        // for users table
        $superAdmin = [
            'company_id' => 1,
            'user_name' => 'SuperAdmin',
            'password' => Hash::make('admin123'),
            'role_id' => SUPER_ADMIN_ROLE_ID,
            'email' => 'superadmin@gmail.com',
            'notice_target' => 0,
            'notice_yourself' => 1,
            'avail_flg' => 1,
            'create_at' => now(),
            'update_at' => now(),
        ];

        $admin = [
            'company_id' => 3,
            'user_name' => 'Admin',
            'password' => Hash::make('admin123'),
            'role_id' => ADMIN_ROLE_ID,
            'email' => 'admin@gmail.com',
            'notice_target' => 0,
            'notice_yourself' => 1,
            'avail_flg' => 1,
            'create_at' => now(),
            'update_at' => now(),
        ];
        DB::table('m_users')->insert([$superAdmin, $admin]);

    }
}
