<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'company_id' => 1,
        'role_id' => USER_ROLE_ID,
        'user_name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => Hash::make('admin123'),
        'notice_target' => 0,
        'notice_yourself' => 1,
        'avail_flg' => 1,
        'create_at' => now(),
        'update_at' => now(),
    ];
});
