<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMapTagProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_tag_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tag_id');
            $table->integer('project_id');
            $table->timestamp('create_at')->default(now());
            $table->timestamp('update_at')->default(now());
            $table->index(['tag_id', 'project_id'], 'IDX1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('map_tag_projects');
    }
}
