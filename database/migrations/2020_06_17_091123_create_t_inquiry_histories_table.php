<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTInquiryHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_inquiry_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inquiry_id');
            $table->integer('edit_type')->nullable();
            $table->integer('comment_id')->nullable();
            $table->timestamp('create_at')->default(now());
            $table->integer('create_by')->nullable();
            $table->timestamp('update_at')->default(now());
            $table->integer('update_by')->nullable();
            $table->index('inquiry_id', 'IDX1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_inquiry_histories');
    }
}
