<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMClassifiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_classifies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('classify_name', 20)->nullable();
            $table->tinyInteger('avail_flg')->default(1)->nullable();
            $table->timestamp('create_at')->default(now());
            $table->timestamp('update_at')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_classifies');
    }
}
