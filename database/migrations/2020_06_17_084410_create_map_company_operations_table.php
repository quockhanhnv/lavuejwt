<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMapCompanyOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_company_operations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_company_id');
            $table->integer('child_company_id');
            $table->timestamp('create_at')->default(now());
            $table->timestamp('update_at')->default(now());
            $table->index('parent_company_id', 'IDX1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('map_company_operations');
    }
}
