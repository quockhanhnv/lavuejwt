<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name', 30)->nullable();
            $table->text('note')->nullable();
            $table->tinyInteger('avail_flg')->default(1)->nullable();
            $table->timestamp('create_at')->default(now());
            $table->timestamp('update_at')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_companies');
    }
}
