<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->string('project_name', 30)->nullable();
            $table->text('note')->nullable();
            $table->tinyInteger('avail_flg')->default(1)->nullable();
            $table->timestamp('create_at')->default(now());
            $table->timestamp('update_at')->default(now());
            $table->index('company_id', 'IDX1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_projects');
    }
}
