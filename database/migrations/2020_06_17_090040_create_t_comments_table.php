<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inquiry_id')->nullable();
            $table->text('comment')->nullable();
            $table->tinyInteger('delete_flg')->default(0)->nullable();
            $table->timestamp('create_at')->default(now());
            $table->integer('create_by')->nullable();
            $table->timestamp('update_at')->default(now());
            $table->integer('update_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_comments');
    }
}
