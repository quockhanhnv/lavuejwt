<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTInquiryHistoryDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_inquiry_history_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('history_id')->nullable();
            $table->integer('property')->nullable();
            $table->string('new_value', 30)->nullable();
            $table->string('old_value', 30)->nullable();
            $table->timestamp('create_at')->default(now());
            $table->integer('create_by')->nullable();
            $table->timestamp('update_at')->default(now());
            $table->integer('update_by')->nullable();
            $table->index('history_id', 'IDX1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_inquiry_history_details');
    }
}
