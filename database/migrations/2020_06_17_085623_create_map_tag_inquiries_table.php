<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMapTagInquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_tag_inquiries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tag_id');
            $table->integer('inquiry_id');
            $table->timestamp('create_at')->default(now());
            $table->timestamp('update_at')->default(now());
            $table->index(['tag_id', 'inquiry_id'], 'IDX1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('map_tag_inquiries');
    }
}
