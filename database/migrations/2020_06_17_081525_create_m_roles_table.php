<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('role_name', 30)->nullable();
            $table->string('description', 100)->nullable();
            $table->timestamp('create_at')->default(now());
            $table->timestamp('update_at')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_roles');
    }
}
