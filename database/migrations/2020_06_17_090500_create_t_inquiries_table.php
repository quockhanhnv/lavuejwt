<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTInquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_inquiries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->nullable();
            $table->integer('project_id')->nullable();
            $table->integer('classify_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->string('title', 30);
            $table->text('contents');
            $table->string('target', 30)->nullable();
            $table->integer('task_owner_id')->nullable();
            $table->date('correspond_scheduled_date')->nullable();
            $table->integer('confirmer_id')->nullable();
            $table->timestamp('confirm_at')->nullable();
            $table->tinyInteger('attachment_flg')->default(0)->nullable();
            $table->tinyInteger('delete_flg')->default(0)->nullable();
            $table->timestamp('create_at')->default(now());
            $table->integer('create_by')->nullable();
            $table->timestamp('update_at')->default(now());
            $table->integer('update_by')->nullable();
            $table->index(['company_id', 'project_id', 'status_id'], 'IDX1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_inquiries');
    }
}
