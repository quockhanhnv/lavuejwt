<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->nullable();
            $table->string('user_name', 30)->nullable();
            $table->string('password', 60)->nullable();
            $table->integer('role_id');
            $table->string('email', 50)->nullable();
            $table->integer('notice_target')->default(0)->nullable();
            $table->tinyInteger('notice_yourself')->default(1)->nullable();
            $table->timestamp('last_login')->nullable();
            $table->tinyInteger('avail_flg')->default(1)->nullable();
            $table->timestamp('create_at')->default(now());
            $table->timestamp('update_at')->default(now());
            $table->index('company_id', 'IDX1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_users');
    }
}
