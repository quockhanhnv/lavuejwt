import Vue from 'vue'
import Router from 'vue-router'
import Login from "../pages/Login";
import HomePage from "../pages/HomePage";
import ResetPassword from "../pages/ResetPassword";
import StatusManage from "../pages/StatusManage";
import CompanyManage from "../pages/CompanyManage";

Vue.use(Router);

export default [{
    path: '/',
    component: { render: h => h('router-view') },
    children: [
        {
            path: 'login',
            name: 'Login',
            component: Login,
            meta: {
                title: 'Login'
            },
        },
        {
            path: '/',
            name: 'HomePage',
            component: HomePage,
            meta: {
                title: 'Home Page',
            },
        },
        {
            path: 'reset-password',
            name: 'ResetPassword',
            component: ResetPassword,
            meta: {
                title: 'Reset Password'
            },
        },
        {
            path: 'status-manage',
            name: 'StatusManage',
            component: StatusManage,
            meta: {
                title: 'Status Manage'
            },
        },
        {
            path: '/company-manage',
            name: 'CompanyManage',
            component: CompanyManage,
            meta: {
                title: 'Company Manage',
            },
        },
    ]
}];
