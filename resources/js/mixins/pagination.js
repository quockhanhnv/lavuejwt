export default {
    data() {
        return {
            currentPage: 1,
            totalRecord: 0,
            from: 0,
            to: 0,
            totalPage: 0,
            pageSize: 10
        }
    },

    methods: {
        setDataPagination(data) {
            this.currentPage = data.current_page || 1;
            this.from = data.from || 0;
            this.to = data.to || 0;
            this.totalRecord = data.total || 0;
            this.totalPage = data.last_page || 0;
        }
    }
}
