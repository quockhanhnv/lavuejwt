<?php

const AVAILABLE_FLAG = 1;
const UNAVAILABLE_FLAG = 0;

const AVAILABLE_CHIEF_FLAG = 1;
const UNAVAILABLE_CHIEF_FLAG = 0;

const AVAILABLE_ATTACHMENT_FLAG = 1;
const UNAVAILABLE_ATTACHMENT_FLAG = 0;

// soft delete field

// status table
const AVAIL_FLG = 'avail_flg';
const HIDDEN = 1;
const NOT_HIDDEN = 0;

// inquiry table
const SOFT_DELETE_COLUMN = 'delete_flg';
const AVAILABLE_DELETE_FLAG = 0; // available
const UNAVAILABLE_DELETE_FLAG = 1; // deleted

// notification setting for user

const NOTICE_TARGET_ALL = 0;
const NOTICE_TARGET_CHIEF_PROJECT = 1;
const NOTICE_TARGET_NONE = 2;

// pagination
const DEFAULT_PAGE_SIZE = 10;
const DEFAULT_SORT_FILED = 'id';
const DEFAULT_SORT_TYPE = 'DESC';

//inquiry history edit_type
const EDIT_TYPE_EDIT_INQUIRY = 1;
const EDIT_TYPE_ADD_COMMENT = 2;
const EDIT_TYPE_EDIT_COMMENT = 3;
const EDIT_TYPE_DELETE_COMMENT = 4;
const EDIT_TYPE_DELETE_INQUIRY = 5;

//inquiry history detail property
const PROP_PROJECT = 1;
const PROP_CLASSIFY = 2;
const PROP_STATUS = 3;
const PROP_TASK_OWNER = 4;
const PROP_SCHEDULED_DATE = 5;
const PROP_CONFIRMER = 6;
const PROP_CONFIRM_AT = 7;
const PROP_TITLE = 8;
const PROP_CONTENTS = 9;
const PROP_ATTACHMENT = 10;
const PROP_TARGET = 11;

const arrProperty = [
    'project_id' => PROP_PROJECT,
    'classify_id' => PROP_CLASSIFY,
    'status_id' => PROP_STATUS,
    'task_owner_id' => PROP_TASK_OWNER,
    'correspond_scheduled_date' => PROP_SCHEDULED_DATE,
    'confirmer_id' => PROP_CONFIRMER,
    'confirm_at' => PROP_CONFIRM_AT,
    'title' => PROP_TITLE,
    'contents' => PROP_CONTENTS,
    'attachment_flg' => PROP_ATTACHMENT,
    'target' => PROP_TARGET,
];

const POST_MAX_FILE_SIZE = 5242880; // 5MB

const SUPER_ADMIN_ROLE_ID = 1;
const ADMIN_ROLE_ID = 2;
const USER_ROLE_ID = 3;

const SUPER_ADMIN_ROLE = 'SuperAdmin';
const ADMIN_ROLE = 'Admin';
const USER_ROLE = 'User';
