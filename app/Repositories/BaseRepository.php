<?php

namespace App\Repositories;
use Illuminate\Database\Eloquent\Model;
use Prophecy\Exception\Doubler\ClassNotFoundException;

abstract class BaseRepository
{
    public $model;

    public function __construct()
    {
        $model = $this->model();
        if (!class_exists($model)) {
            throw new ClassNotFoundException('Class not found', $model);
        }

        $this->model = new $model();
    }

    public abstract function model();

    public function query()
    {
        return $this->model->query();
    }

    public function getAll() {
        return $this->model->get();
    }

    public function create($data) {
        return $this->model->create($data);
    }

    public function insert($data) {
        return $this->model->insert($data);
    }

    public function softDelete(Model $instance, $field, $value)
    {
        $instance->$field = $value;
        return $instance->save();
    }

    public function destroy(Model $instance)
    {
        return $instance->delete();
    }

    public function update($id, $data)
    {
        $result = $this->find($id);

        if ($result) {
            $result->update($data);
            return $result;
        }

        return false;
    }

    public function find($id, $relations = [])
    {
        try {
            return empty($relations)
                ? $this->model->findOrFail($id)
                : $this->model->with($relations)->findOrFail($id);
        } catch (\Exception $e) {
            return false;
        }
    }
}
