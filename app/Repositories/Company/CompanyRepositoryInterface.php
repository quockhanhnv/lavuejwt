<?php

namespace App\Repositories\Company;

interface CompanyRepositoryInterface
{
    public function findById($id);
    public function getAll();
    public function getAllPagination();
    public function getChildren($id);
    public function getChildrenPagination($id);
}
