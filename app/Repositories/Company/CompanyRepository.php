<?php

namespace App\Repositories\Company;

use App\Models\Company;
use App\Models\MapCompanyOperation;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;

class CompanyRepository extends BaseRepository implements CompanyRepositoryInterface
{

    public function model()
    {
        return Company::class;
    }

    public function findById($id)
    {
        return $this->model
            ->select(['id', 'company_name', 'note'])
            ->where([
                ['id', $id],
                ['avail_flg', AVAILABLE_FLAG]
            ])
            ->first();
    }

    public function getAll($sortField = 'company_name', $sortType = 'ASC')
    {
        return $this->model
            ->select(['id', 'company_name', 'note'])
            ->where('avail_flg', AVAILABLE_FLAG)
            ->orderBy($sortField, $sortType)
            ->get();
    }

    public function getAllPagination($perPage = 10, $sortField = 'company_name', $sortType = 'ASC')
    {
        return $this->model
            ->select(['id', 'company_name', 'note'])
            ->where('avail_flg', AVAILABLE_FLAG)
            ->orderBy($sortField, $sortType)
            ->paginate((int) $perPage)
            ->toArray();
    }

    public function getChildren($companyId)
    {
        $parent = $this->findById($companyId);

        if(!$parent)
            return [];

        return $parent->children()
            ->orderBy('company_name')
            ->where('avail_flg', AVAILABLE_FLAG)
            ->get()
            ->toArray();
    }

    public function getChildrenPagination($companyId, $perPage = 10, $sortField = 'company_name', $sortType = 'ASC')
    {
        $parent = $this->findById($companyId);

        if(!$parent)
            return [];

        return $parent->children()
            ->orderBy('company_name')
            ->where('avail_flg', AVAILABLE_FLAG)
            ->orderBy($sortField, $sortType)
            ->paginate((int) $perPage)
            ->toArray();
    }
}
