<?php

namespace App\Repositories\MapCompanyOperation;

interface MapCompanyOperationRepositoryInterface
{
    public function getCompanies();
}