<?php

namespace App\Repositories\MapCompanyOperation;

use App\Models\MapCompanyOperation;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;

class MapCompanyOperationRepository extends BaseRepository implements MapCompanyOperationRepositoryInterface
{

    public function model()
    {
        return MapCompanyOperation::class;
    }

    public function getCompanies()
    {
        return $this->model->with('getChildrenName')->where('parent_company_id', Auth::user()->company_id);
    }
}