<?php

namespace App\Repositories\Status;

use App\Models\Status;
use App\Repositories\BaseRepository;

class StatusRepository extends BaseRepository implements StatusRepositoryInterface
{
    public function model()
    {
        return Status::class;
    }

    public function getAll($orderField = 'status_name', $orderType = 'ASC')
    {
        return $this->model->select('id', 'status_name', 'color_code')
            ->where('avail_flg', AVAILABLE_FLAG)
            ->orderBy($orderField, $orderType)
            ->get();
    }

    public function getStatusForInquiries()
    {
        return $this->model->where(AVAIL_FLG, AVAILABLE_FLAG)->get();
    }
}
