<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StatusRequest;
use App\Models\Status;
use App\Services\StatusService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    use ResponseTrait;

    protected $statusService;

    public function __construct(StatusService $statusService)
    {
        // $this->middleware('check.role:'.basename(__FILE__, '.php'));
        $this->statusService = $statusService;
    }

    public function index(Request $request)
    {
        $statuses = $this->statusService->search($request);

        return $this->responseSuccess(__('get_api_successfully'), [
            'statuses' => $statuses['data'],
            'pagination' => [
                "current_page" => $statuses['current_page'],
                'from' => $statuses['from'],
                'per_page' => $statuses['per_page'],
                'to' => $statuses['to'],
                'total' => $statuses['total'],
                'last_page' => $statuses['last_page']
            ]
        ]);
    }

    public function store(StatusRequest $request)
    {
        $data = $request->only(['status_name', 'color_code']);
        $status = $this->statusService->create($data);

        if($status)
            return $this->responseSuccess(__('created_successfully'), $status);

        return $this->responseWithAnonymousException();
    }

    public function update(StatusRequest $request, $id)
    {
        $data = $request->only(['status_name', 'color_code']);
        $status = $this->statusService->update($id, $data);

        if(is_null($status))
            return $this->responseNotFound(__('data_not_found'));

        if($status)
            return $this->responseSuccess(__('updated_successfully'), $status);

        return $this->responseWithAnonymousException();
    }

    public function destroy($id)
    {
        $status = $this->statusService->softDelete($id);

        if(is_null($status))
            return $this->responseNotFound(__('data_not_found'));

        if($status)
            return $this->responseSuccess(__('deleted_successfully'));

        return $this->responseWithAnonymousException();
    }
}
