<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use App\Services\UserService;


class UserController extends Controller
{
    use ResponseTrait;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index()
    {
        $users = $this->userService->getAll();
        if (!$users) {
            return $this->responseWithAnonymousException();
        }

        return $this->responseSuccess(__('get_api_successfully'), $users);

    }
}
