<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CompanyRequest;
use App\Services\CompanyService;
use App\Services\UploadFileService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CompanyController extends Controller
{
    use ResponseTrait;

    protected $companyService;
    protected $uploadFileService;

    public function __construct(CompanyService $companyService, UploadFileService $uploadFileService)
    {
//        $this->middleware('check.role:'.basename(__FILE__, '.php'));
        $this->companyService = $companyService;
        $this->uploadFileService = $uploadFileService;
    }

    public function index(Request $request)
    {

        $data = $this->companyService->getByRole($request);

        return $this->responseSuccess('get_api_successfully', [
            'companies' => $data['data'] ?? [],
            'pagination' => [
                'current_page' => $data['current_page'] ?? 1,
                'from' => $data['from'] ?? 0,
                'per_page' => $data['per_page'] ?? DEFAULT_PAGE_SIZE,
                'to' => $data['to'] ?? 0,
                'total' => $data['total'] ?? 0,
                'last_page' => $data['last_page'] ?? 1
            ]
        ]);
    }

    public function show($id)
    {
        $company = $this->companyService->findById($id);

        if($company)
            return $this->responseSuccess('get_api_successfully', $company);

        return $this->responseNotFound();
    }

    public function store(CompanyRequest $request)
    {
        $data = $request->only(['company_name', 'note']);

        DB::beginTransaction();
        try {
            $company = $this->companyService->create($data);
            if($company) {
                if($request->hasFile('attachment_file')) {
                    $files = $request->attachment_file;
                    foreach ($files as $file) {
                        $this->uploadFileService->uploadAttachFile($file, $company->id);
                    }
                }
                DB::commit();

                return $this->responseSuccess('created_successfully', $company);
            }
        } catch (\Exception $exception) {
            Log::error('Internal Server ' . $exception->getMessage());

            DB::rollBack();
            return $this->responseWithAnonymousException();
        }
    }

    public function update(CompanyRequest $request, $id)
    {
        $data = $request->only(['company_name', 'note']);

        $company = $this->companyService->findAndUpdate($id, $data);

        if($request->hasFile('attachment_file')) {
            $files = $request->attachment_file;
            foreach ($files as $file) {
                $this->uploadFileService->uploadAttachFile($file, $company->id);
            }
        }

        if(is_null($company))
            return $this->responseNotFound();

        if($company)
            return $this->responseSuccess('updated_successfully', $company);

        return $this->responseWithAnonymousException();
    }

    public function destroy($id)
    {
        $company = $this->companyService->softDelete($id);

        if(is_null($company))
            return $this->responseNotFound();

        if($company)
            return $this->responseSuccess('deleted_successfully');

        return $this->responseWithAnonymousException();
    }

    public function getCompanies(Request $request)
    {
        return $this->responseSuccess(
            __('get_api_successfully'),
            $this->companyService->getCompaniesForListInquiry()
        );
    }

    public function getFiles($id)
    {
        return $this->companyService->getFiles($id);
    }

    public function download($id, Request $request)
    {
        $file_path = public_path("uploads/$id/") . $request->file_name;

        if (!file_exists($file_path))
        {
            return $this->responseWithAnonymousException();
        }

        return response()->download($file_path, $request->filename, [
            'Content-Type: application/*',
        ]);
    }

    public function deleteFile($id, Request $request)
    {
        $file_path = public_path("uploads/$id/") . $request->file_name;

        if (unlink($file_path))
            return $this->responseSuccess();

        return $this->responseWithAnonymousException();
    }

}
