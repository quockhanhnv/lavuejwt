<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "status_name" => "required|max:30|unique:m_statuses,status_name,{$this->id},id,avail_flg," . AVAILABLE_FLAG,
            "color_code" => "max:10"
        ];
    }
}
