<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Repositories\Status\StatusRepositoryInterface;

class StatusService extends BaseService
{
    public function __construct(StatusRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function search(Request $request)
    {
        $pageSize = $request->query('page_size') ?? DEFAULT_PAGE_SIZE;
        $sortFiled = $request->query('sort_field') ?? DEFAULT_SORT_FILED;
        $sorType = $request->query('sort_type') ?? DEFAULT_SORT_TYPE;

        return $this->repository->query()
            ->select('id', 'status_name', 'color_code')
            ->where([
                ['avail_flg', '=', AVAILABLE_FLAG],
                ['hidden_flg', '=', NOT_HIDDEN]
            ])
            ->orderBy($sortFiled, $sorType)
            ->paginate($pageSize)
            ->toArray();
    }

    public function getAll($orderField = 'status_name', $orderType = 'ASC')
    {
        return $this->repository->getAll($orderField = 'status_name', $orderType = 'ASC');
    }

    public function findById($id = '')
    {
        return $this->repository->query()
            ->where([
                ['id', $id],
                ['avail_flg', AVAILABLE_FLAG]
            ])
            ->first();
    }

    public function update($id, $data)
    {
        $status = $this->findById($id);

        if(!$status)
            return null;

        return $status->fill($data)->save();
    }

    public function softDelete($id)
    {
        $status = $this->findById($id);

        if(!$status)
            return null;

        $status->avail_flg = UNAVAILABLE_FLAG;
        return $status->save();
    }

    public function getStatusForInquiries()
    {
        return $this->repository->getStatusForInquiries();
    }
}
