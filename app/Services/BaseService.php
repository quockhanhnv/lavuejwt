<?php

namespace App\Services;

use Exception;
use Illuminate\Support\Facades\Log;

class BaseService
{
    protected $repository;
    protected $guard = 'api';

    public function create($data) {
        return $this->repository->create($data);
    }

    public function insert($data) {
        return $this->repository->insert($data);
    }

    public function getAll() {
        return $this->repository->get();
    }

    public function find($id, $relations = []) {
        return $this->repository->find($id, $relations);
    }

    public function findAndUpdate($id, $data = []) {
        try {
            if (!$instance = $this->find($id)) return null;

            if ($instance instanceof $this->repository->model) {
                $instance->fill($data)->save();
            }

            return $instance;
        } catch (Exception $exception) {
            Log::error('Something went wrong ' . __METHOD__ . ' with : ' . $exception->getMessage());
            return null;
        }
    }

    public function findAndSoftDelete($id, $field, $value) {
        if (!$instance = $this->find($id)) {
            return false;
        }

        if ($instance instanceof $this->repository->model) {
            $delete = $this->repository->softDelete($instance, $field, $value);
            if ($delete) return $instance;
        }

        return false;
    }

    public function findAndDelete($id) {
        if (!$instance = $this->find($id)) {
            return false;
        }

        if ($instance instanceof $this->repository->model) {
            $delete = $this->repository->destroy($instance);
            if ($delete) return $instance;
        }

        return false;
    }

    protected function role() {
        $user = auth($this->guard)->user();
        $role = $user->role;
        return strtolower($role->role_name ?? "");
    }

    public function isSuperAdmin() {
        return $this->role() === strtolower(SUPER_ADMIN_ROLE);
    }

    public function isAdmin() {
        return $this->role() === strtolower(ADMIN_ROLE);
    }

    public function isUser() {
        return $this->role() === strtolower(USER_ROLE);
    }

}
