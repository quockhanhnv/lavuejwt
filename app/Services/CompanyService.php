<?php

namespace App\Services;

use App\Repositories\Company\CompanyRepositoryInterface;
use App\Repositories\MapCompanyOperation\MapCompanyOperationRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class CompanyService extends BaseService
{
    protected $mapCompanyOperation;

    public function __construct(CompanyRepositoryInterface $repository, MapCompanyOperationRepositoryInterface $mapCompanyOperation)
    {
        $this->repository = $repository;
        $this->mapCompanyOperation = $mapCompanyOperation;
    }

    public function getByRole($request)
    {
        $pageSize = $request->page_size ?? DEFAULT_PAGE_SIZE;
        $sortField = $request->sort_field ?? DEFAULT_SORT_FILED;
        $sortType = $request->sort_type ?? DEFAULT_SORT_TYPE;

        if ($this->isSuperAdmin())
            return $this->repository->getAllPagination($pageSize, $sortField, $sortType);
        elseif ($this->isAdmin()) {
            $companyId = Auth::user()->company_id;
            return $this->repository->getChildrenPagination($companyId, $pageSize, $sortField, $sortType);
        }

        return [];
    }

    public function getCompaniesForListInquiry()
    {
        if ($this->isSuperAdmin())
            return $this->repository->getAll();
        elseif ($this->isAdmin()) {
            $companyId = Auth::user()->company_id;
            return $this->repository->getChildren($companyId);
        }
        return $this->findById(Auth::user()->company_id);
    }

    public function create($data)
    {
        DB::beginTransaction();

        try {
            $newCompany = $this->repository->create($data);

            $this->mapCompanyOperation->create([
               'parent_company_id' => $newCompany->id,
               'child_company_id' => $newCompany->id
            ]);

            $this->mapCompanyOperation->create([
               'parent_company_id' => Auth::user()->company_id,
               'child_company_id' => $newCompany->id
            ]);

            DB::commit();

            return $newCompany;
        } catch (\Exception $e) {
            DB::rollBack();

            return false;
        }
    }

    public function getAllPagination($perPage = 10, $sortField = 'company_name', $sortType = 'ASC')
    {
        return $this->repository->getAllPagination($perPage, $sortField, $sortType);
    }

    public function findById($id = '')
    {
        return $this->repository->query()
            ->select('id', 'company_name', 'note')
            ->where([
                ['id', $id],
                ['avail_flg', AVAILABLE_FLAG]
            ])
            ->first();
    }

    public function update($id, $data)
    {
        $company = $this->findById($id);

        if(!$company)
            return null;

        return $company->fill($data)->save();
    }

    public function softDelete($id)
    {
        $company = $this->findById($id);

        if(!$company)
            return null;

        $company->avail_flg = UNAVAILABLE_FLAG;
        return $company->save();
    }

    public function getFiles($id)
    {
        $path = public_path("uploads/$id");

        if(!file_exists($path)) {
            return [];
        }
        $allFiles = File::allFiles($path);
        $files = [];
        foreach ($allFiles as $file) {
            $files[] = $file->getFilename();
        }
        return $files;
    }
}
