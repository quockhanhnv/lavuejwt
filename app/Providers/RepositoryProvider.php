<?php

namespace App\Providers;

use App\Repositories\Company\CompanyRepository;
use App\Repositories\Company\CompanyRepositoryInterface;
use App\Repositories\MapCompanyOperation\MapCompanyOperationRepository;
use App\Repositories\MapCompanyOperation\MapCompanyOperationRepositoryInterface;
use Illuminate\Support\ServiceProvider;

use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Status\StatusRepository;
use App\Repositories\Status\StatusRepositoryInterface;

class RepositoryProvider extends ServiceProvider
{

    public $bindings = [
        UserRepositoryInterface::class => UserRepository::class,
        StatusRepositoryInterface::class => StatusRepository::class,
        CompanyRepositoryInterface::class => CompanyRepository::class,
        MapCompanyOperationRepositoryInterface::class => MapCompanyOperationRepository::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
