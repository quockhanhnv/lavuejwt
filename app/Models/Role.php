<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\RoleRight;

class Role extends Model
{
    protected $table = 'm_roles';

    const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';

    public function RoleRights()
    {
        return $this->hasMany(RoleRight::class, 'role_id');
    }

}
