<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'm_statuses';

    protected $fillable = ['status_name', 'color_code', 'hidden_flg'];

    const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';

    public function inquires()
    {
        return $this->hasMany(Inquiry::class);
    }
}
