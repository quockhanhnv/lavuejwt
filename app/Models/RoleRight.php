<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleRight extends Model
{
    protected $table = 'm_role_rights';

    const CREATED_AT = 'create_at';
    const UPDATED_AT = null;

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }
}
