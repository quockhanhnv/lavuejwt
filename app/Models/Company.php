<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'm_companies';

    protected $fillable = ['company_name', 'note'];

    const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';

    public function inquires()
    {
        return $this->hasMany(Inquiry::class, 'company_id');
    }

    public function projects()
    {
        return $this->hasMany(Project::class, 'company_id')
            ->where('avail_flg', AVAILABLE_FLAG);
    }

    public function users()
    {
        return $this->hasMany(User::class, 'company_id');
    }

    public function children()
    {
        return $this->belongsToMany(self::class, 'map_company_operations', 'parent_company_id', 'child_company_id')
            ->select('m_companies.id', 'company_name', 'note')
            ->where('avail_flg', AVAILABLE_FLAG);
    }

    public function parent()
    {
        return $this->belongsToMany(self::class, 'map_company_operations', 'child_company_id', 'parent_company_id');
    }
}
