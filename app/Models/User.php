<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $table = 'm_users';

    const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'user_name', 'email', 'password', 'role_id', 'email', 'notice_target', 'notice_yourself', 'last_login', 'avail_flg'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id')->select(['id', 'role_name']);
    }

    public function listAcceptControllers()
    {
        $role = $this->role;
        $roleRights = $role ? $role->roleRights : null;
        return $roleRights ? $roleRights->pluck('controller')->toArray() : [];
    }

    public function isSuperAdmin()
    {
        $roleName = strtolower($this->role ? $this->role->role_name : "");
        return strtolower(SUPER_ADMIN_ROLE_ID) === $roleName;
    }

    public function isAdmin()
    {
        $roleName = strtolower($this->role ? $this->role->role_name : "");
        return strtolower(ADMIN_ROLE_ID) === $roleName || $this->isSuperAdmin();
    }

    public function isUser()
    {
        $roleName = strtolower($this->role ? $this->role->role_name : "");
        return strtolower(USER_ROLE_ID) === $roleName || $this->isAdmin();
    }
}
