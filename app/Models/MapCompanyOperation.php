<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MapCompanyOperation extends Model
{
    protected $table = 'map_company_operations';

    protected $fillable = ['parent_company_id', 'child_company_id'];

    const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';

    public function parent()
    {
        return $this->belongsTo(self::class,'parent_company_id', 'parent_company_id');
    }

    public function children()
    {
        return $this->hasMany(self::class,'child_company_id', 'child_company_id');
    }

    public function getChildrenName()
    {
        return $this->belongsTo(Company::class, 'child_company_id');
    }
}
